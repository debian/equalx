/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFINES_H
#define DEFINES_H

#define APP_FULL_NAME "EqualX - The LaTeX Equation Editor"
#define APP_NAME "EqualX"
#define APP_VERSION "0.7.1"
#define APP_SITE "http://equalx.sourceforge.net/"
#define AUTHOR_NAME "Mihai Niculescu"
#define AUTHOR_EMAIL "q.quark@gmail.com"

/*
*   DEFAULT FACTORY SETTINGS for QSettings
*/
// GENERAL defaults
#define SETTINGS_DIR "equalx"
#define SETTINGS_FILE "settings"
#define DEFAULT_REMEMBER_LAYOUT true
#define DEFAULT_WINDOW_WIDTH 400
#define DEFAULT_WINDOW_HEIGHT 500
#define DEFAULT_WINDOW_X 200
#define DEFAULT_WINDOW_Y 200
#define DEFAULT_SHOW_TOOLBAR true
#define DEFAULT_SHOW_MAIN_TOOLBAR true
#define DEFAULT_SHOW_PANEL_SYMBOLS true
#define DEFAULT_SHOW_PANEL_TEMPLATES true
#define DEFAULT_SHOW_PANEL_PROPERTIES true
#define DEFAULT_SHOW_PANEL_SIDEBAR false
#define DEFAULT_EXPORT_TYPE "SVG"

// EDITOR defaults
#ifdef UNIX_PLATFORM
#define DEFAULT_FONT "Monospace,10,-1,5,50,0,0,0,0,0"
#endif

#ifdef WIN_PLATFORM
#define DEFAULT_FONT "Arial,10,-1,5,50,0,0,0,0,0"
#endif
// represents the index in the font combobox
#define DEFAULT_FONT_SIZE 4
#define DEFAULT_HIGHLIGHTING true
#define DEFAULT_WRAPPING true
#define DEFAULT_COMPLETION true

// PREVIEW defaults
#define DEFAULT_UPDATE_AUTO true
#define DEFAULT_UPDATE_TIME 1500
#define DEFAULT_PREVIEW_BG "transparent"
#define DEFAULT_RENDER_FG "black"
#define DEFAULT_RENDER_BG "transparent"
// represents the index in the font combobox
#define DEFAULT_RENDER_FONT_SIZE LATEX_FONT_NORMAL
#define DEFAULT_RENDER_MODE LATEX_ENV_MODE_DISPLAY

// LATEX
#ifdef UNIX_PLATFORM
#define DEFAULT_PDFLATEX "/usr/bin/pdflatex"
#define DEFAULT_PDFLATEX_ARGS "-interaction=nonstopmode"
#define DEFAULT_PDFCAIRO "/usr/bin/pdftocairo"
#define DEFAULT_PDFCAIRO_ARGS ""
#define DEFAULT_GS "/usr/bin/gs"
#define DEFAULT_GS_ARGS ""

//where to look for symbols.ini and the actual symbols
#define SYMBOLS_PATH "/usr/share/equalx/resources/symbols"
#endif

#ifdef WIN_PLATFORM
#define DEFAULT_PDFLATEX "pdflatex.exe"
#define DEFAULT_PDFLATEX_ARGS "-interaction=nonstopmode"
#define DEFAULT_PDFCAIRO "poppler\\pdftocairo.exe"
#define DEFAULT_PDFCAIRO_ARGS ""
#define DEFAULT_GS "gs.exe"
#define DEFAULT_GS_ARGS ""

#define SYMBOLS_PATH "resources/symbols"
#endif

// environment for the latex equation
enum LATEX_ENV_MODE{
    LATEX_ENV_MODE_DISPLAY,
    LATEX_ENV_MODE_INLINE,
    LATEX_ENV_MODE_ALIGN,
    LATEX_ENV_MODE_TEXT
};

enum LATEX_FONT_SIZE{
    LATEX_FONT_TINY=0,
    LATEX_FONT_SCRIPT,
    LATEX_FONT_FOOTNOTE,
    LATEX_FONT_SMALL,
    LATEX_FONT_NORMAL,
    LATEX_FONT_LARGE,
    LATEX_FONT_VERY_LARGE,
    LATEX_FONT_HUGE,
    LATEX_FONT_VERY_HUGE
};

/*
 * defines for parsing the .tex file
 */
#define TEMP_FILE_NAME "equation"
#define TEMP_LATEX_FILE "equation.tex"
#define TEMP_LATEX_CROP_FILE "equation-cropped.tex"
#define TEMP_PDF_FILE "equation.pdf"
#define TEMP_PDFCROP_FILE "equation-cropped.pdf"
#define TEMP_METADATA_FILE "metadata.xmp"

// METADATA
#define METADATA_NS "http://ns.equalx/meta"
// XMP schema
#define METADATA_PREFIX "equalx"
#define METADATA_PREAMBLE "preamble"
#define METADATA_EQUATION "equation"
#define METADATA_ENV "environment"
#define METADATA_FG "color"
#define METADATA_BG "background"
#define METADATA_DOC_FONT_SIZE "fontsize"



#endif // DEFINES_H
