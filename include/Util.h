/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H
#define UTIL_H

class QString;
class QColor;

namespace EqualX{

namespace Util{

// compute font resolution from Font Size in Points
int getFontResolution(int fontSizePt);

// computer the scale factor from the zoom slider
// NOTE: this assumes the slider varies from (-10,10) and scale=1.0 for value 0
float getScaleFactor(int sliderValue);

bool colorToLatex(const QColor &fromColor, QString &toLatexStr);
void LatexToColor(const char* fromColor,QColor &toColor);

QColor oppositeColor(const QColor& color); // returns the complementary color

}

}
#endif // UTIL_H
