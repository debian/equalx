/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EQUATIONPIXMAPITEM_H
#define EQUATIONPIXMAPITEM_H

#include <QLabel>


class QMouseEvent;

class EquationView : public QLabel
{
public:
    EquationView(QWidget * parent=0);

    void clear();
    QSize sizeHint() const;
    QString exportSource() const;

    void setExportSource(const QString& filename);
    void setPixmap(const QString &fileName);

    void show();
protected: // Methods
    void mousePressEvent( QMouseEvent *event );
    void mouseMoveEvent( QMouseEvent *event );
    void paintEvent(QPaintEvent *);
protected: // Data
    QPointF mDragPosition;
    QString mDragFileSource;
    QSize  mAnimStartSize;
    QSize  mAnimEndSize;
};

#endif // EQUATIONPIXMAPITEM_H
