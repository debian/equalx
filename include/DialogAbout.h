/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGABOUT_H
#define DIALOGABOUT_H

#include <QDialog>
#include "ui_dialogAbout.h"

class DialogAbout : public QDialog, public Ui::DialogAboutUI
{
    Q_OBJECT

public:
    DialogAbout(QWidget *parent);
    virtual ~DialogAbout();

public slots:
    void onClickedLicenseButton(bool show=true);
    void onClickedThanksButton(bool show=true);

};

#endif // DIALOGABOUT_H
