/*
    This file is part of EqualX, based on LineEdit from Martin Rotter http://www.martin-rotter.8u.cz/2013/03/qlineedit-subclass-with-clear-button-in-english/

    EqualX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EqualX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EqualX.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2012 - 2013 Martin Rotter
    Copyright 2014 Mihai Niculescu
*/

/****************************************************************************
**
** Copyright (c) 2007 Trolltech ASA <info@trolltech.com>
**
** Use, modification and distribution is allowed without limitation,
** warranty, liability or support of any kind.
**
****************************************************************************/

#ifndef LINEEDIT_H
#define LINEEDIT_H

#include <QToolButton>
#include <QLineEdit>


class SearchLineEdit : public QLineEdit {
    Q_OBJECT

  public:
    // Constructors and destructors
    SearchLineEdit(QWidget *parent = 0);
    ~SearchLineEdit();

  public slots:
    // Disables or enables clear button.
    void setClearButtonEnabled(bool enable);

    // Tweak default implementations.
    void setEnabled(bool enable);
    void setReadOnly(bool read_only);

  protected slots:
    void onTextChanged(const QString &new_text);

  protected:
    // Places clear button into the correct position.
    void resizeEvent(QResizeEvent *event);

    // Returns width of QLineEdit frame.
    int frameWidth() const;

  private:
    QToolButton *mClearButton;
    bool mClearButtonEnabled;
    QCompleter *mCompleter;
};

#endif // LIENEDIT_H
