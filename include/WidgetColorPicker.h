/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WIDGETCOLORPICKER_H_
#define __WIDGETCOLORPICKER_H_

#include <QComboBox>
#include <QColor>


class WidgetColorPicker : public QComboBox
{
    Q_OBJECT
    
public: //Methods
    WidgetColorPicker(QWidget *parent = 0);
    ~WidgetColorPicker();

    QColor getColor() const;
    QString getColorName() const;

    void setCurrentIndex(int index);

    /* setColor(const QColor& color)
     * setColorName(const QString& colorName);
     *
     * try to select this color if it is in the list, otherwise add a new color
     * it will emit signals:
     *  - currentIndexChanged(int index)
     *  - colorSelectionChanged(QString name)
     *  - colorSelectionChanged(QColor color)
     *
     * color name is a Qt named color
     */
    void setColor(const QColor& color);
    void setColorName(const QString& colorName);
private: //Methods
    void init();
    QIcon createIcon(const QString& colorName) const;
    void addColor(QString colorName);
    void addColor(QColor color);

private: //Data Members
    QColor m_Color;
    QString m_ColorName;
    int m_currentColorIndex;

private slots: //Slots
    void on_comboBox_currentIndexChanged(int index);

signals: //Signal
    void colorSelectionChanged(QString);
    void colorSelectionChanged(QColor);
};

#endif /*__WIDGETCOLORPICKER_H_*/
