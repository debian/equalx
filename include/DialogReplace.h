/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGFINDNREPLACE_H
#define DIALOGFINDNREPLACE_H

#include <QDialog>
#include <QTextDocument>
#include "ui_dialogReplace.h"

class DialogReplace : public QDialog, public Ui::DialogReplace {
    Q_OBJECT
public:
    DialogReplace(QWidget *parent = 0);
    ~DialogReplace();

    QString getFindExpr() const;
    QString getReplaceExpr() const;
    QTextDocument::FindFlags getFindOptions() const;

    void setFindExpr(const QString &expr) { findEntry->setText(expr); }

public slots:
    void onFindEntryChanged(const QString &findExpr);
    void onReplaceEntryChanged(const QString &);

    void show();

protected slots:
    void onClosing();
    void onClickedFindButton();
    void onClickedReplaceButton();
    void onClickedReplaceAllButton();

signals:
    void find(const QString expr, QTextDocument::FindFlags flags);
    void replace(const QString expr);
    void replaceAll(const QString expr);
    void closing();

protected:
    void changeEvent(QEvent *e);

    void init();
    void setSignals();

};

#endif // DIALOGFINDNREPLACE_H
