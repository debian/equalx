/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILEINFO_H
#define FILEINFO_H

#include <QObject>
#include <QColor>
#include <QString>

#include "defines.h"
#include "HLSelections.h"

namespace EqualX{

/*
 * A EqualX File is any file which contains EqualX metadata embedded as XMP
 *
 * This class only describes the metadata specific for a EqualX file. For reading/writing EqualX Files look in File.h
 *
 */
class FileInfo : public QObject
{
    Q_OBJECT
public:
    explicit FileInfo(QObject *parent = 0);
    FileInfo(const EqualX::FileInfo& other);

    QString equation() const;
    QString preamble() const;
    HLSelections selections() const;
    LATEX_FONT_SIZE fontSize() const;
    QColor fgColor() const;
    QColor bgColor() const;
    LATEX_ENV_MODE environment() const;
    
    void setEquation(const QString& equation);
    void setPreamble(const QString& preamble);
    void setSelections(const HLSelections& selections);
    EqualX::FileInfo& operator=(const EqualX::FileInfo& other);

public slots:
    void setFontSize(int size); // size is enum LATEX_FONT_SIZE
    void setForegroundColor(const QColor& color);
    void setBackgroundColor(const QColor& color);
    void setEnvironment(int env); // int values must be correlated with RENDER_MODE
    
private:
    QString mPreamble;
    QString mEquation;
    LATEX_FONT_SIZE mFontSize; // font size
    QColor mFGColor; // color is in format for latex color
    QColor mBGColor; // color is in format for latex color
    HLSelections mSelections;
    LATEX_ENV_MODE mEnvironment; // latex environment for equation
};

}
#endif // FILEINFO_H
