/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QProcess>
#include <QTimer>
#include <QTime>

#include "defines.h"
#include "Symbol.h"
#include "LibraryData.h"

class QAction;
class QActionGroup;
class QButtonGroup;
class QCloseEvent;
class QMenu;
class QModelIndex;
class QMovie;
class QPushButton;
class QToolButton;
class QSignalMapper;
class QProcess;
class QStandardItemModel;
class QPlainTextEdit;

class DialogPreferencesBookmark;
class DialogPreferences;
class DialogReplace;
class DialogAbout;
class EquationView;
class LatexEditor;
class WidgetFind;
class SearchLineEdit;
class LibraryManager;

namespace EqualX {
    class RenderEngine;
    class FileInfo;
}


namespace Ui
{
class MainWindowClass;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setWindowModified(bool mod=true);

public slots:
    void updateEquation(); // updates tex, png, and scene

    void onFindTextChanged(const QString &exp);

    void showFindDialog();
    void showReplaceDialog();
    void showLatexOutputWindow() const;
    void insertSymbol(LatexSymbol *symbol);

    void copyEquationToClipboard();
    void pasteEquationFromClipboard();

    void onActivatedHistoryRow(const LibraryModelData &row);
    void onActivatedBookmark(int bookmarkId);
    void onBookmarkAdd();

    void showBookmarks();
    void showHistory();

private slots:
    void equationChanged();

    void newEquation();
    void onAbout();
    void onDeleteSelection(); // delete selected text from editor
    void onPreferences(); // show preferences dialog
    void open();

    bool saveAs();

    void loadEquation(const QString &fileName);

    void preferencesAccepted();
    void renderStarted();
    void renderFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void renderError(QProcess::ProcessError);

    void writeSettings(); //read all settings from preferencesDialog and write them to the Settings File
    void readSettings(); // read all settings from the Settings File and update preferencesDialog

protected:
    void showEquation();
    void clearCurrentHighlight();
    void setActions();
    void setSymbolsPanel();
    void setMathPanelTabsActions();

    void setSignals();
    void setCustomStyleSheets();

    bool saveEquation(const QString &fileName);

    void closeEvent(QCloseEvent *);


    /*
        Adds ToolButtons with Actions conforming the file found by file name at eqImage
    to the widget
    */
    void addButtonToMathTabs(const QString &eqImage, QWidget *widget);
    void addButtonToMathTabs(const QStringList &list, QWidget *widget);

    void updateUIFromSettings(); // update Main Window UI according to settings
    QString strippedName(const QString &fullFileName);

private:
    void checkRequirements(); // check if the requirements are fulfilled

    /*  MainWindow widgets */
    Ui::MainWindowClass *ui;

    EqualX::FileInfo *mEquationData;
    EquationView *mEquationView;
    LatexEditor *mLatexEditor;
    WidgetFind *mFindWidget;
    QButtonGroup* renderModeButtonGroup;
    QMovie* mAnimationProgress;
    QTimer mAutoUpdateTimer;
    SearchLineEdit* mSearchLineEdit;
    QPlainTextEdit* mLatexOutput;
    LibraryManager* mLibrary;

    QActionGroup* mExportActionsGroup;
    EqualX::RenderEngine* mRenderer;

    QStandardItemModel* mSymbolsModel; // the symbols model used for completion in editor and searcher

    // Dialogs
    DialogReplace *mDialogReplace;
    DialogPreferences *mDialogPreferences;
    DialogAbout *mDialogAbout;
    DialogPreferencesBookmark *mDialogBookmark;
    Bookmark mCurrentBookmark; // holds the loaded bookmark from library (and which is currently displayed)

    // data
    QString curSaveDir; // current file

    // flags
    bool highlightAll;
};

#endif // MAINWINDOW_H
