/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGPREFERENCES_H
#define DIALOGPREFERENCES_H

#include <QDialog>
#include "ui_dialogPreferences.h"

class QAbstractButton;
class QButtonGroup;

class DialogPreferences : public QDialog, public Ui::DialogPreferencesUI
{
    Q_OBJECT

    enum PREFERENCES_ITEM{
        ITEM_GENERAL=0,
        ITEM_EDITOR,
        ITEM_PREVIEW,
        ITEM_ADVANCED
    };

public:
    DialogPreferences(QWidget *parent=0);
    virtual ~DialogPreferences();

    const QFont getFont() const;
    int renderMode() const;

    QString currentExportType() const;

public slots:
    void setCurrentExportType(const QString &fileType);
    void setCurrentExportType(QAction *actionType);
    void setCurrentFont(const QString &fontDescription);

private slots:
    void onClickedButtonBox(QAbstractButton* b);
    void onBrowsePdfLatex();
    void onBrowsePdfToCairo();
    void onBrowseGS();

    void onPreferencesItemActivated(QListWidgetItem *item);
protected:
    void resetDefaults();

private:
    void setupPreferencesListWidget();

    QButtonGroup* renderModeButtonGroup;

};

#endif // DIALOGPREFERENCES_H
