/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOOKMARKSWIDGET_H
#define BOOKMARKSWIDGET_H

#include <QModelIndex>
#include <QWidget>
#include <QTimer>

namespace Ui {
class BookmarksWidget;
}

class QStandardItemModel;
class QActionGroup;
class QAction;

class BookmarksItemModel;
class DialogPreferencesFolder;
class DialogPreferencesBookmark;

class LibraryManager;
class LibraryModelData;

class BookmarksWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit BookmarksWidget(QWidget *parent = 0);
    ~BookmarksWidget();
    
public slots:
    void onDoubleClick(const QModelIndex& index);

    void onOpen();
    void onNewFolder();
    void onCut();
    void onCopy();
    void onPaste();
    void onDelete();
    void onProperties();

    void resetAutoSearchTimer(const QString&);

protected slots:
    void searchBookmarks();

signals:
    void activated(int);

protected:
    void setupContextMenuActions();
    void setupBookmarksSearchFields();

    /*
    void dragEnterEvent(QDragEnterEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *);
    void dropEvent(QDropEvent *ev);
    */
private:
    Ui::BookmarksWidget *ui;
    BookmarksItemModel* mModel;
    QStandardItemModel* mSearchModel;
    DialogPreferencesFolder* mDgPreferencesFolder;
    DialogPreferencesBookmark* mDgPreferencesBookmark;
    QActionGroup * mActionsGroupSearchBy;
    QAction* mActionSearchByContent;

    QModelIndex mCopiedRow;
    bool mIsCutActive;

    QTimer mAutoSearchTimer;
};

#endif // BOOKMARKSWIDGET_H
