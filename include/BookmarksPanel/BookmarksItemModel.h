/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOOKMARKSITEMMODEL_H
#define BOOKMARKSITEMMODEL_H

#include <QAbstractItemModel>
#include <QModelIndexList>

#include "Library/LibraryData.h"

class BookmarkItem;
class LibraryManager;

class BookmarksItemModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    BookmarksItemModel(LibraryManager* lib, QObject *parent = 0);
    ~BookmarksItemModel();

    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const;
    bool canFetchMore(const QModelIndex &parent) const;
    void fetchMore(const QModelIndex &parent);
    bool hasChildren(const QModelIndex &parent) const;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;

    BookmarkItem* invisibleRootItem() const;
    BookmarkItem* itemFromIndex(const QModelIndex &index) const;

    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    bool insertRow(int row, const QModelIndex &parent);
    bool insertRows(int row, int count, const QModelIndex &parent); // insert undefined rows into model, but not in library (use setData to make them valid)
    bool removeRow(int row, const QModelIndex & parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent);

    // drag n drop support
    Qt::DropActions supportedDropActions() const;
    Qt::DropActions supportedDragActions() const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

    // Specific methods to this model
    QVariant getCompleteData(const QModelIndex &index) const; // returns complete data from DB of this index

public slots:
    void onFolderChanged(const QString &dirPath);
private:
    QModelIndex findItem(const QString &dirPath);

    BookmarkItem *mRootItem;
    LibraryManager *mLibrary;
};

#endif // BOOKMARKSITEMMODEL_H
