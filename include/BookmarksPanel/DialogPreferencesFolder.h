/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGPREFERENCESFOLDER_H
#define DIALOGPREFERENCESFOLDER_H

#include <QDialog>

namespace Ui {
class DialogPreferencesFolder;
}

class DialogPreferencesFolder : public QDialog
{
    Q_OBJECT
    
public:
    explicit DialogPreferencesFolder(QWidget *parent = 0);
    ~DialogPreferencesFolder();
    
    const QString name() const;
    const QString description() const;

    void setName(const QString& name);
    void setDescription(const QString& description);

private:
    Ui::DialogPreferencesFolder *ui;
};

#endif // DIALOGPREFERENCESFOLDER_H
