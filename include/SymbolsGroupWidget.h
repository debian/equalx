/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WIDGETTOOLMENU_H_
#define __WIDGETTOOLMENU_H_

#include <QFrame>
#include <Symbol.h>

class QAction;
class QHBoxLayout;
class QToolButton;
class QSignalMapper;

class SymbolsGroupMenu;

class SymbolsGroupWidget : public QFrame
{
    Q_OBJECT

public: // Methods
    SymbolsGroupWidget(QWidget * parent = 0);
    SymbolsGroupWidget(int nVisibleCols, QWidget * parent = 0);
    virtual ~SymbolsGroupWidget();

    void addSymbols(LatexSymbolsGroup* gr);
    void setIconSize(const QSize &size);

public slots:
    void showMenu();


protected: // methods
    void init(int ncols);
    void addButton(QWidget *button);
    void addSymbol(LatexSymbol* sym);

private: // data Members

    QHBoxLayout *mLayout; // layout used in container widget

    LatexSymbolsGroup* mSymbolsGroup;
    SymbolsGroupMenu *mMenu;
    QToolButton *mShowMenuButton;
    QSignalMapper *mSignalMapper;

    QSize mIconSize;

    int mMaxCols; // total number of visible columns shown by widget
    int mCurCol;



protected:
    // virtual void mousePressEvent(QMouseEvent *event);
    virtual void enterEvent(QEvent *);
    virtual void leaveEvent(QEvent *);
private slots:
    void symbolTriggered(int id);

signals:
    void triggered(LatexSymbol*);

};

#endif // WIDGETTOOLMENU_H
