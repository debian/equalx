# -------------------------------------------------
# Project created by QtCreator 2009-01-30T14:36:03
# -------------------------------------------------

message(Qt version: $$[QT_VERSION])

CONFIG += qt gui

CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix{
DEFINES += UNIX_PLATFORM
CONFIG+= link_pkgconfig
PKGCONFIG += exempi-2.0
INCLUDEPATH+=/usr/include/exempi-2.0
LIBS += -L/usr/lib
OTHER_FILES+=equalx.1

SOURCES =  src/File.cpp
}

win32{
DEFINES += WIN_PLATFORM
RC_FILE = resources/resourceIconWindows.rc
OTHER_FILES+=resources/resourceIconWindows.rc
INCLUDEPATH+="C:/XMPSDK/include"
LIBS+=C:/XMPSDK/libraries/XMPCore.lib C:/XMPSDK/libraries/XMPFiles.lib

SOURCES =  src/FileWin.cpp
}

QT += widgets sql

TARGET = equalx
TEMPLATE = app

UI_HEADERS_DIR = include
UI_SOURCES_DIR = src
MOC_DIR = tmp
RCC_DIR = src

INCLUDEPATH += include \
    include/Library \
    include/BookmarksPanel \
    include/HistoryPanel

DEPENDPATH = include/ \
    include/Library \
    include/BookmarksPanel \
    include/HistoryPanel \
    src \
    ui

INSTALLS += target
DISTFILES += resources/templates/* \
    resources/icons/menu/* \
    resources/icons/tabBar/* \
    resources/icons/mathbar/*
SOURCES += src/main.cpp \
    src/MainWindow.cpp \
    src/LatexHighlighter.cpp \
    src/LatexEditor.cpp \
    src/WidgetColorPicker.cpp \
    src/DialogReplace.cpp \
    src/DialogPreferences.cpp \
    src/DialogAbout.cpp \
    src/WidgetFind.cpp \
    src/EquationTemplateWidget.cpp \
    src/Util.cpp \
    src/RenderEngine.cpp \
    src/EquationView.cpp \
    src/FileInfo.cpp \
    src/EquationArea.cpp \
    src/SymbolsGroupWidget.cpp \
    src/SymbolsGroupMenu.cpp \
    src/SymbolsPanel.cpp \
    src/SearchLineEdit.cpp \
    src/Symbol.cpp \
    src/ColorChooser.cpp \
    src/Library/LibraryData.cpp \
    src/Library/Library.cpp \
    src/BookmarksPanel/DialogPreferencesFolder.cpp \
    src/BookmarksPanel/DialogPreferencesBookmark.cpp \
    src/BookmarksPanel/BookmarksWidget.cpp \
    src/BookmarksPanel/BookmarksViewItemDelegate.cpp \
    src/BookmarksPanel/BookmarksView.cpp \
    src/BookmarksPanel/BookmarksItemModel.cpp \
    src/BookmarksPanel/BookmarkItem.cpp \
    src/HistoryPanel/HistoryWidget.cpp \
    src/HistoryPanel/HistoryViewItemDelegate.cpp \
    src/HistoryPanel/HistoryView.cpp \
    src/HistoryPanel/HistoryListModel.cpp
HEADERS += include/MainWindow.h \
    include/defines.h \
    include/LatexHighlighter.h \
    include/LatexEditor.h \
    include/WidgetColorPicker.h \
    include/DialogReplace.h \
    include/DialogPreferences.h \
    include/DialogAbout.h \
    include/WidgetFind.h \
    include/EquationTemplateWidget.h \
    include/Util.h \
    include/RenderEngine.h \
    include/HLSelections.h \
    include/EquationView.h \
    include/File.h \
    include/FileInfo.h \
    include/EquationArea.h \
    include/SymbolsGroupWidget.h \
    include/SymbolsGroupMenu.h \
    include/SymbolsPanel.h \
    include/Symbol.h \
    include/SearchLineEdit.h \
    include/ColorChooser.h \
    include/Library/LibraryData.h \
    include/Library/Library.h \
    include/BookmarksPanel/DialogPreferencesFolder.h \
    include/BookmarksPanel/DialogPreferencesBookmark.h \
    include/BookmarksPanel/BookmarksWidget.h \
    include/BookmarksPanel/BookmarksViewItemDelegate.h \
    include/BookmarksPanel/BookmarksView.h \
    include/BookmarksPanel/BookmarksItemModel.h \
    include/BookmarksPanel/BookmarkItem.h \
    include/HistoryPanel/HistoryWidget.h \
    include/HistoryPanel/HistoryViewItemDelegate.h \
    include/HistoryPanel/HistoryView.h \
    include/HistoryPanel/HistoryListModel.h
FORMS += ui/mainwindow.ui \
    ui/dialogPreferences.ui \
    ui/dialogAbout.ui \
    ui/dialogReplace.ui \
    ui/widgetFind.ui \
    ui/BookmarksPanel/DialogPreferencesFolder.ui \
    ui/BookmarksPanel/DialogPreferencesBookmark.ui \
    ui/BookmarksPanel/BookmarksWidget.ui \
    ui/HistoryPanel/HistoryWidget.ui
RESOURCES += resourceFiles.qrc
OTHER_FILES += LICENSE \
    COPYING \
    README \
    THANKS \
    changelog \
    resources/stylesheet.qss \
    resources/createLibraryTables.sql
