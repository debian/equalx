/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DialogReplace.h"
#include "ui_dialogReplace.h"

DialogReplace::DialogReplace(QWidget *parent) :
        QDialog(parent)
{
    setupUi(this);

    init();
}

DialogReplace::~DialogReplace()
{

}

void DialogReplace::init()
{
    setSignals();

}

void DialogReplace::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}

void DialogReplace::onClosing()
{
    emit closing();

    QDialog::close();
}

QString DialogReplace::getFindExpr() const
{
    return findEntry->text();
}

QString DialogReplace::getReplaceExpr() const
{
    return replaceEntry->text();
}

QTextDocument::FindFlags DialogReplace::getFindOptions() const
{
    QTextDocument::FindFlags opts;

    if( checkCase->isChecked() ) opts = QTextDocument::FindCaseSensitively;
    if( checkBackwards->isChecked() ) opts = opts | QTextDocument::FindBackward;
    if( checkWholeWords->isChecked() ) opts = opts | QTextDocument::FindWholeWords;

    return opts;
}

void DialogReplace::onFindEntryChanged(const QString &findExpr)
{
    bool findable = !findExpr.isEmpty();

    findButton->setEnabled( findable );
    replaceButton->setEnabled( findable );
    replaceAllButton->setEnabled( findable );
}

void DialogReplace::onReplaceEntryChanged(const QString& /*replaceExpr*/)
{

}

void DialogReplace::onClickedFindButton()
{
    QString expr = findEntry->text();
    QTextDocument::FindFlags searchFlags;

    if( checkCase->isChecked() ) searchFlags = searchFlags | QTextDocument::FindCaseSensitively;
    if( checkBackwards->isChecked() ) searchFlags = searchFlags | QTextDocument::FindBackward;
    if( checkWholeWords->isChecked() ) searchFlags = searchFlags | QTextDocument::FindWholeWords;

    emit find(expr, searchFlags);
}

void DialogReplace::onClickedReplaceButton()
{
     QString expr = replaceEntry->text();

    emit replace(expr);
}

void DialogReplace::onClickedReplaceAllButton()
{
    QString expr = replaceEntry->text();

    emit replaceAll(expr);
}

void DialogReplace::setSignals()
{
    connect(findEntry, SIGNAL(textChanged(QString)), this, SLOT(onFindEntryChanged(QString)) );
    connect(replaceEntry, SIGNAL(textChanged(QString)), this, SLOT(onReplaceEntryChanged(QString)) );
    connect(findButton, SIGNAL(clicked()), this, SLOT(onClickedFindButton()) );
    connect(replaceButton, SIGNAL(clicked()), this, SLOT(onClickedReplaceButton()) );
    connect(replaceAllButton, SIGNAL(clicked()), this, SLOT(onClickedReplaceAllButton()) );
    connect(closeButton, SIGNAL(clicked()), this, SLOT(onClosing()) );

}

void DialogReplace::show()
{
    QDialog::show();

    findEntry->setFocus();

}

