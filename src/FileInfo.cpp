/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include "FileInfo.h"

EqualX::FileInfo::FileInfo(QObject *parent) :
    QObject(parent),
    mPreamble(),
    mEquation(),
    mFontSize(LATEX_FONT_NORMAL),
    mFGColor(Qt::black),
    mBGColor(Qt::transparent),
    mSelections(),
    mEnvironment(LATEX_ENV_MODE_DISPLAY)
{
}

EqualX::FileInfo::FileInfo(const EqualX::FileInfo &other)
    : QObject(0)
{
    mPreamble = other.preamble();
    mEquation = other.equation();
    mFontSize = other.fontSize();
    mFGColor    = other.fgColor();
    mBGColor    = other.bgColor();
    mSelections = other.selections();
    mEnvironment= other.environment();
}

QString EqualX::FileInfo::equation() const
{
    return mEquation;
}

QString EqualX::FileInfo::preamble() const
{
    return mPreamble;
}

HLSelections EqualX::FileInfo::selections() const
{
    return mSelections;
}

LATEX_FONT_SIZE EqualX::FileInfo::fontSize() const
{
    return mFontSize;
}

QColor EqualX::FileInfo::fgColor() const
{
    return mFGColor;
}

QColor EqualX::FileInfo::bgColor() const
{
    return mBGColor;
}

LATEX_ENV_MODE EqualX::FileInfo::environment() const
{
    return mEnvironment;
}

void EqualX::FileInfo::setEquation(const QString &equation)
{
    mEquation = equation;
}

void EqualX::FileInfo::setPreamble(const QString &preamble)
{
    mPreamble = preamble;
}

void EqualX::FileInfo::setSelections(const HLSelections &selections)
{
    mSelections = selections;
}

void EqualX::FileInfo::setFontSize(int size)
{
    mFontSize = (LATEX_FONT_SIZE)size;
}

void EqualX::FileInfo::setForegroundColor(const QColor &color)
{
    qDebug() << "FG Color changed to " << color.name();
    mFGColor = color;
}

void EqualX::FileInfo::setBackgroundColor(const QColor &color)
{
    qDebug() << "BG Color changed to " << color.name();
    mBGColor = color;
}

void EqualX::FileInfo::setEnvironment(int env)
{
     mEnvironment = (LATEX_ENV_MODE)env;
}

EqualX::FileInfo &EqualX::FileInfo::operator =(const EqualX::FileInfo &other)
{
    if(&other==this) return *this;

    mPreamble = other.preamble();
    mEquation = other.equation();
    mFontSize = other.fontSize();
    mFGColor    = other.fgColor();
    mBGColor    = other.bgColor();
    mSelections = other.selections();
    mEnvironment= other.environment();

    return *this;
}

