/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDesktopWidget>
#include <QColor>
#include <QString>

#include "defines.h"
#include "Util.h"

namespace EqualX{

int Util::getFontResolution(int fontSizePt)
{
    int fontResolution;
    int dpi = QApplication::desktop()->logicalDpiX();

    fontResolution =  int(fontSizePt*dpi*72.27/720);

    return fontResolution;
}

float Util::getScaleFactor(int sliderValue)
{
    float scale=1.0;

    if(sliderValue==0){
        scale = (float)0.1;
    }
    else{
        scale = (float)sliderValue/10.0;
    }

    return scale;

}


bool Util::colorToLatex(const QColor &fromColor, QString &toLatexStr)
{
    // handle case for alpha - latex does not show alpha background
    if(fromColor.alpha()==0){
        toLatexStr.clear();
        return true;
    }

    toLatexStr = QString("%1,%2,%3").arg(QString::number(fromColor.redF()), QString::number(fromColor.greenF()), QString::number(fromColor.blueF()) );

    return true;
}

void  Util::LatexToColor(const char* fromColor,QColor &toColor)
{
    QString fromLatexColor(fromColor);

    if(fromLatexColor.isEmpty() || fromLatexColor.isNull()){
        toColor = QColor(Qt::transparent);
        return;
    }

    QStringList sl = fromLatexColor.split(",", QString::SkipEmptyParts);
    toColor.setRedF(sl.at(0).toFloat());
    toColor.setGreenF(sl.at(1).toFloat());
    toColor.setBlueF(sl.at(2).toFloat());

 }

QColor Util::oppositeColor(const QColor& color)
{
    QColor op =color.toHsl();
    int h, s, l;

    op.getHsl(&h,&s,&l);

    h+= 180;
    s=1;
    l=1;
    op.setHsl(h,s,l);
    op.toRgb();

    return op;
}

} // end namespace EqualX
