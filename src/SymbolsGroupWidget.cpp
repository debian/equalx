/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QDebug>

#include <QAction>
#include <QHBoxLayout>
#include <QToolButton>
#include <QTimer>
#include <QSignalMapper>

#include "SymbolsGroupWidget.h"
#include "SymbolsGroupMenu.h"
#include "defines.h"

SymbolsGroupWidget::SymbolsGroupWidget(QWidget *parent)
    : QFrame(parent)
{
    init(1);
}

SymbolsGroupWidget::SymbolsGroupWidget( int nVisibleCols, QWidget *parent)
    : QFrame(parent)
{
    init(nVisibleCols);

    mSignalMapper = new QSignalMapper(this);
    connect(mSignalMapper, SIGNAL(mapped(int)), this, SLOT(symbolTriggered(int)));
}

SymbolsGroupWidget::~SymbolsGroupWidget()
{

}

void SymbolsGroupWidget::init(int ncols)
{
    mSymbolsGroup = 0;

    setFrameStyle(QFrame::Panel | QFrame::Sunken);

    mMaxCols = ncols;
    mCurCol = 0;

    mIconSize = QSize(20, 20);

    mLayout = new QHBoxLayout;
    mLayout->setContentsMargins(3 , 3, 3, 3);
    mLayout->setSpacing(0);

    
    mMenu = new SymbolsGroupMenu(this, mMaxCols);

    mShowMenuButton = new QToolButton(this);
    mShowMenuButton->setMaximumWidth(15);
    mShowMenuButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    mShowMenuButton->setArrowType(Qt::DownArrow);
    mShowMenuButton->setPopupMode(QToolButton::InstantPopup);
    mShowMenuButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
    mShowMenuButton->setAutoRaise(true);
    mShowMenuButton->setVisible(false);

    QHBoxLayout *hbox = new QHBoxLayout(this);
    hbox->setContentsMargins(0, 0,0,0);
    hbox->setSpacing(0);

    hbox->addLayout(mLayout);
    hbox->addSpacerItem(new QSpacerItem(3,3, QSizePolicy::Preferred));
    hbox->addWidget(mShowMenuButton);

    setLayout(hbox);

    connect(mShowMenuButton, SIGNAL(clicked()), this, SLOT(showMenu()) );


}

void SymbolsGroupWidget::addSymbol(LatexSymbol *symbol)
{
    QToolButton * button = new QToolButton(this);
    button->setAutoRaise(true);
    button->setIconSize(mIconSize);
    symbol->icon = QString("%1/%2/%3").arg( SYMBOLS_PATH, symbol->parent->name, symbol->icon);

    button->setIcon(QIcon(symbol->icon) );
    button->setText(symbol->name);
    button->setToolTip(QString("<html><head/><body><div><img src=\"%5\" width=\"20\" style=\"background-color:white\"></div><p><b>Name:</b> %1</p><p><b>Group:</b>%2</p><p><b>Latex:</b>%3</p><p><b>Package:</b>%4</p></body></html>").arg(symbol->name, symbol->parent->name, symbol->latex, symbol->packages.join(","), symbol->icon ));

    mSignalMapper->setMapping(button, symbol->id);
    connect(button, SIGNAL(clicked()), mSignalMapper, SLOT(map()) );

    addButton(button);
}

void SymbolsGroupWidget::addSymbols(LatexSymbolsGroup* gr)
{
    mSymbolsGroup = gr;
    for(int i=0; i < gr->symbols.size(); ++i){
        addSymbol( gr->symbols.at(i) );
    }
}

void SymbolsGroupWidget::addButton(QWidget *button)
{
    // first add Button to widget
    if(mCurCol < mMaxCols){
        // add this button in widget
        mLayout->addWidget(button);


    } else {
        // add this button in Menu
        mMenu->appendWidget(button);
        mShowMenuButton->setVisible(true);

    }
    mCurCol++;

}

void SymbolsGroupWidget::symbolTriggered(int id)
{
    LatexSymbol* symbol = mSymbolsGroup->symbols.at(id);

    emit triggered(symbol);
}

void SymbolsGroupWidget::showMenu()
{

    if(!mMenu->isEmpty() && mMenu->isHidden() ){
        mMenu->popup();

    }

}


void SymbolsGroupWidget::enterEvent(QEvent* /*event*/)
{
    if(mMenu->isVisible()) mMenu->showMenu();
}

void SymbolsGroupWidget::leaveEvent(QEvent* /*event*/)
{
    QTimer::singleShot(200, mMenu, SLOT(hideMenu()) );
}
