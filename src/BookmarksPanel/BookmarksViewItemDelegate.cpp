/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include <QApplication>
#include <QDateTime>
#include <QPainter>
#include <QVariant>

#include "Library/LibraryData.h"
#include "BookmarksPanel/BookmarksViewItemDelegate.h"

BookmarksViewItemDelegate::BookmarksViewItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
    widthH = 80;
    heightH= 80;
}

QWidget *BookmarksViewItemDelegate::createEditor(QWidget* /* parent*/, const QStyleOptionViewItem& /*&option*/, const QModelIndex& /*  index */) const
{
    return 0;
}

void BookmarksViewItemDelegate::setEditorData(QWidget* /* editor */, const QModelIndex& /*  index */) const
{
}

void BookmarksViewItemDelegate::setModelData(QWidget* /* editor */, QAbstractItemModel* /*model*/, const QModelIndex& /*  index */) const
{
}

void BookmarksViewItemDelegate::updateEditorGeometry(QWidget* /* editor */, const QStyleOptionViewItem& /*option*/, const QModelIndex& /*  index */) const
{
}

void BookmarksViewItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex& index) const
{
    LibraryModelData data = qvariant_cast<LibraryModelData>(index.data(Qt::DisplayRole));

    painter->save();

    painter->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform, true);
    painter->setPen(Qt::SolidLine);

    QRect drawArea = option.rect;

    if (option.state & QStyle::State_Selected){
        painter->fillRect(drawArea, option.palette.highlight());
    }

    if(data.isBookmark()){
        QImage eq(data.name);

        int w = eq.width();
        int h = eq.height();

        float af = (float)w/(float)h; // aspect ratio
        int wmax = drawArea.height()*af; // represents the maximum width to draw at maximum possible height (and keep aspect ratio)


        if(drawArea.width()>=wmax){
            w = qMin(wmax,w);
            h = qMin(drawArea.height(),h);
        } else {
            w = qMin(drawArea.width(),w);
            h = qMin(int(w/af),h);
        }

        int x =  drawArea.x();
        int y = (drawArea.height()- h)/2.0 + drawArea.y();

        eq = eq.scaled(QSize(w, h), Qt::KeepAspectRatio, Qt::SmoothTransformation);

        painter->setPen(option.palette.buttonText().color());
        painter->drawImage(x,y, eq);
    }

    if(data.isFolder()){

        QIcon icon= QIcon::fromTheme("folder", QIcon("://resources/icons/bookmarks/folder.png"));

        QRect textBB(option.fontMetrics.boundingRect(data.name)); // text bounding box
        int titleH = textBB.height();

        QRect titleArea(drawArea.x()+titleH+3, drawArea.y()+3, drawArea.width()-3,titleH+3);

        if (option.state & QStyle::State_Selected){
            painter->setPen(option.palette.highlightedText().color());
        }
        else {
            painter->setPen(option.palette.windowText().color());
        }

        painter->setFont(option.font);
        painter->drawText(titleArea, Qt::AlignLeft, data.name);
        icon.paint(painter, drawArea.x(),drawArea.y(), titleH, titleH);
    }
    painter->restore();
}

QSize BookmarksViewItemDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    LibraryModelData data = qvariant_cast<LibraryModelData>(index.data());

    if(data.isBookmark()){
        LibraryModelData data = qvariant_cast<LibraryModelData>(index.data(Qt::DisplayRole));
        QPixmap pix(data.name);

        return QSize(qMin(widthH, pix.size().width()), heightH);
    }

    QRect r = option.fontMetrics.boundingRect(data.name);

    return QSize(r.width(),r.height()+6);

}
