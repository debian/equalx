/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDrag>
#include <QList>
#include <QMimeData>
#include <QMouseEvent>
#include <QBuffer>
#include <QPainter>
#include <QPixmap>
#include <QUrl>

#include "defines.h"
#include "EquationView.h"

EquationView::EquationView(QWidget * parent)
    : QLabel(parent),
      mDragPosition(),
      mDragFileSource()
{
    setMinimumSize(64,64); // set a minimum size to have displayed correctly the preloader
    setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    setAcceptDrops(true);
    setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    setPixmap("");
}

void EquationView::clear()
{
    QLabel::clear();
    unsetCursor();
}

QSize EquationView::sizeHint() const
{
    if(pixmap()) return pixmap()->size();

    return QLabel::sizeHint();
}

QString EquationView::exportSource() const
{
    return mDragFileSource;
}

void EquationView::setExportSource(const QString &filename)
{
    mDragFileSource = filename;
}

void EquationView::setPixmap(const QString &fileName)
{
    // a safe value if the dragfilesource wasn't specified with setExportSource()
    mDragFileSource = fileName;

    QPixmap pix;

    if(pix.load(fileName)){
        QLabel::setPixmap(pix);
        setCursor(Qt::OpenHandCursor);
    }
    else{
        clear();
    }

}

void EquationView::mousePressEvent( QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton ){
        mDragPosition = event->pos();
    }

}

void EquationView::mouseMoveEvent( QMouseEvent * event)
{
    if (!(event->buttons() & Qt::LeftButton))
        return;

    if (QLineF(event->pos(), mDragPosition).length() < QApplication::startDragDistance()) {
        return;
    }

    if(!pixmap()) return;

    QList<QUrl> urls;
    QUrl fileUrl = QUrl::fromLocalFile(mDragFileSource);

    urls.append( fileUrl );


    QMimeData *mimeData = new QMimeData;
    mimeData->setUrls(urls);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap( *pixmap() );
    drag->setHotSpot( event->pos() - QPoint((width()-pixmap()->width())/2.0, (height()-pixmap()->height())/2.0 ) );
    drag->exec(Qt::CopyAction);
}

void EquationView::paintEvent(QPaintEvent * ev)
{
    QLabel::paintEvent(ev);
    if(pixmap()) resize(pixmap()->size());
}
