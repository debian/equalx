/*
 * Copyright 2014 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDateTime>
#include <QFileInfo>

#include "Library/LibraryData.h"

//______________LibraryModelData_____________________________

LibraryModelData::LibraryModelData()
    : id(0),
      idparent(0),
      name(""),
      type(TypeFolder)
{
    created = QDateTime::currentMSecsSinceEpoch();

}

LibraryModelData::LibraryModelData(const LibraryModelData &other)
{
    id = other.id;
    idparent = other.idparent;
    name = QString(other.name);
    type = (LibraryModelData::Type)other.type;
    created = other.created;
}

LibraryModelData::~LibraryModelData()
{
}

bool LibraryModelData::isFolder() const
{
    return (type==TypeFolder);
}

bool LibraryModelData::isBookmark() const
{
    return (type==TypeBookmark);
}

bool LibraryModelData::isHistory() const
{
    return (type==TypeHistory);
}

bool LibraryModelData::isValid() const
{
    return (id>0);
}

QDebug operator<<(QDebug dbg, const LibraryModelData &data)
{
    QString output  = QString("LibraryModelData( id:%1 idparent:%2 name:[%3] type:%4 created:%5 )").arg(data.id).arg(data.idparent).arg(data.name).arg(data.type).arg(data.created);
    dbg.nospace() << output;

    return dbg.maybeSpace();
}



//______________History Row_____________________________

HistoryRow::HistoryRow()
    : id(0),
      title(""),
      filePath("")
{
    created = QDateTime::currentMSecsSinceEpoch();

}

HistoryRow::HistoryRow(const HistoryRow &other)
    : id(other.id),
      title(other.title),
      filePath(other.filePath),
      created(other.created)
{

}

HistoryRow::~HistoryRow()
{
}

LibraryModelData HistoryRow::basicData() const
{
    LibraryModelData basic;
    basic.id = id;
    basic.idparent=0;
    basic.name= filePath;
    basic.type= LibraryModelData::TypeHistory;
    basic.created= created;

    return basic;
}

//_______________Bookmark Row______________________________

Bookmark::Bookmark()
    : id(0),
      idparent(0),
      title(""),
      description(""),
      filePath(""),
      dirPath("")
{
    created = QDateTime::currentMSecsSinceEpoch();
    modified= created;
}

Bookmark::Bookmark(const Bookmark& other)
    : id(other.id),
      idparent(other.idparent),
      title(other.title),
      description(other.description),
      filePath(other.filePath),
      dirPath(other.dirPath),
      created(other.created),
      modified(other.modified)
{

}

Bookmark::Bookmark(const LibraryModelData &data)
    : id(data.id),
      idparent(data.idparent),
      title(""),
      description(""),
      filePath(data.name),
      dirPath(""),
      created(data.created),
      modified(data.created)
{

}

Bookmark::~Bookmark()
{ }

LibraryModelData Bookmark::basicData() const
{
    LibraryModelData basic;
    basic.id = id;
    basic.idparent=idparent;
    basic.name= filePath;
    basic.type= LibraryModelData::TypeBookmark;
    basic.created= created;

    return basic;
}

bool Bookmark::isValid() const
{
    return (id>0);
}

QString Bookmark::fileExt() const
{
    QFileInfo finfo(filePath);
    return finfo.completeSuffix();
}

void Bookmark::clear()
{
    id = 0;
    idparent=0;
    title= "";
    description="";
    dirPath="";
    filePath="";

}

//_______________Bookmark Folder Row______________________________

BookmarkFolder::BookmarkFolder()
    : id(0),
      idparent(0),
      name(""),
      description(""),
      dirPath("")
{
    created = QDateTime::currentMSecsSinceEpoch();
}

BookmarkFolder::BookmarkFolder(const BookmarkFolder& other)
    : id(other.id),
      idparent(other.idparent),
      name(other.name),
      description(other.description),
      dirPath(other.dirPath),
      created(other.created)
{
}

BookmarkFolder::BookmarkFolder(const LibraryModelData &data)
    : id(data.id),
      idparent(data.idparent),
      name(data.name),
      description(""),
      dirPath(""),
      created(data.created)
{

}

BookmarkFolder::~BookmarkFolder()
{ }

LibraryModelData BookmarkFolder::basicData() const
{
    LibraryModelData basic;
    basic.id = id;
    basic.idparent=idparent;
    basic.name= name;
    basic.type= LibraryModelData::TypeFolder;
    basic.created= created;

    return basic;
}

bool BookmarkFolder::isValid() const
{
    return (id>0);
}
