/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPropertyAnimation>
#include <QFile>
#include <QTextStream>

#include "DialogAbout.h"

#include "defines.h"

DialogAbout::DialogAbout(QWidget *parent)
    : QDialog(parent)
{
    setupUi(this);

    QString appName = labelAppName->text();
    labelAppName->setText(appName.arg(APP_NAME));

    QString appFullName = labelAppFullName->text();
    labelAppFullName->setText(appFullName.arg(APP_FULL_NAME));

    QString appVersion = labelAppVersion->text();
    labelAppVersion->setText(appVersion.arg(APP_VERSION));

    frameLicense->setVisible(false);
    labelMore->setTextFormat(Qt::RichText);
    labelMore->setWordWrap(true);

    connect(licenseButton, SIGNAL(released()), this, SLOT(onClickedLicenseButton()) );
    connect(thanksButton, SIGNAL(released()), this, SLOT(onClickedThanksButton()) );

    adjustSize();
}

DialogAbout::~DialogAbout()
{

}

void DialogAbout::onClickedLicenseButton(bool show)
{
    if(show){
        QString licenseText;

        QFile file(":/LICENSE");
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            licenseText = stream.readAll();
            stream.flush();
            file.close();

            labelMore->setText(licenseText);
        }
        frameLicense->setVisible(show);

        adjustSize();
    }
}

void DialogAbout::onClickedThanksButton(bool show)
{
    if(show){
        QString thanksText;

        QFile file(":/THANKS");
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            thanksText = stream.readAll();
            stream.flush();
            file.close();

            labelMore->setText(thanksText);
        }
        frameLicense->setVisible(show);

        adjustSize();
    }
}
