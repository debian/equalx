/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include <QColorDialog>
#include <QStandardItemModel>

#include "WidgetColorPicker.h"

WidgetColorPicker::WidgetColorPicker(QWidget *parent)
    : QComboBox(parent)
{
    init();
}

WidgetColorPicker::~WidgetColorPicker()
{

}

void WidgetColorPicker::init()
{
    setMaxCount(21);

    disconnect(this,SIGNAL(activated(int)),this,SLOT(on_comboBox_currentIndexChanged(int)));
    addItem("Custom Color..");
    addItem(createIcon("transparent"),"transparent",QColor(Qt::transparent));
    addItem(createIcon("#000000"),"black",QColor("#000000"));
    addItem(createIcon("#FFFFFF"),"white",QColor("#FFFFFF"));
    addItem(createIcon("#FF0000"),"red",QColor("#FF0000"));
    addItem(createIcon("#800000"),"dark red",QColor("#800000"));
    addItem(createIcon("#00ff00"),"green",QColor("#00ff00"));
    addItem(createIcon("#008000"),"dark green",QColor("#008000"));
    addItem(createIcon("#0000ff"),"blue",QColor("#0000ff"));
    addItem(createIcon("#000080"),"dark blue",QColor("#000080"));
    addItem(createIcon("#00ffff"),"cyan",QColor("#00ffff"));
    addItem(createIcon("#ff00ff"),"magenta",QColor("#ff00ff"));
    addItem(createIcon("#ffff00"),"yellow",QColor("#ffff00"));
    addItem(createIcon("#a0a0a4"),"gray",QColor("#a0a0a4"));
    addItem(createIcon("#808080"),"dark gray",QColor("#808080"));
    addItem(createIcon("#c0c0c0"),"light gray",QColor("#c0c0c0"));

    m_Color = QColor(0,0,0,0);
    m_ColorName = "transparent";
    setCurrentIndex(1);
    connect(this,SIGNAL(currentIndexChanged(int)),this,SLOT(on_comboBox_currentIndexChanged(int)));
}

QIcon WidgetColorPicker::createIcon(const QString &colorName) const
{
    QPixmap iconPixmap(32,32);
    iconPixmap.fill(QColor(colorName));
    QIcon itemIcon(iconPixmap);
    return itemIcon;
}

void WidgetColorPicker::on_comboBox_currentIndexChanged(int index)
{
    if(index==0)
    {
        QColor chosen = QColorDialog::getColor(m_Color,this);

        if(!chosen.isValid() || chosen == m_Color){
            setCurrentIndex(m_currentColorIndex);
        }
        else{
            m_Color = chosen;
            m_ColorName = m_Color.name();
            addColor(m_Color);
        }
    }
    else {
        m_ColorName = itemText(index);
        m_Color     = itemData(index).value<QColor>();
        m_currentColorIndex = index;
    }

    emit colorSelectionChanged(m_ColorName);
    emit colorSelectionChanged(m_Color);
}

void WidgetColorPicker::setCurrentIndex(int index)
{
    m_currentColorIndex = index;
    QComboBox::setCurrentIndex(index);
}

void WidgetColorPicker::setColorName(const QString &colorName)
{
    if(!colorName.isEmpty())
    {
        if(QColor(colorName).isValid())
        {
            m_ColorName = colorName;
            m_Color = QColor(colorName);
            addColor(colorName);
        }
    }

}

void WidgetColorPicker::setColor(const QColor& color)
{
    if(color.isValid())
    {
        m_ColorName = color.name();
        m_Color = color;
        addColor(color);
    }
}

QColor WidgetColorPicker::getColor() const
{
    return m_Color;
}

QString WidgetColorPicker::getColorName() const
{
    return m_ColorName;
}

void WidgetColorPicker::addColor(QColor color)
{
    int index;
    if((index = findData(color)) > -1)
        setCurrentIndex(index);
    else
    {
        QString colorName = color.name().toUpper();
        insertItem(1,createIcon(colorName),colorName,color);
        setCurrentIndex(1);
    }
}

void WidgetColorPicker::addColor(QString colorName)
{
    int index;

    if((index = findText(colorName)) > -1)
        setCurrentIndex(index);
    else
    {
        insertItem(1,createIcon(colorName),colorName,QColor(colorName));
        setCurrentIndex(1);
    }
}
