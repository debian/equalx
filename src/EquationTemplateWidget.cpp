/*
 * Copyright 2010 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPushButton>
#include <QSignalMapper>
#include <QHBoxLayout>

#include "defines.h"
#include "EquationTemplateWidget.h"

EquationTemplateWidget::EquationTemplateWidget(QStringList paths, QWidget *parent)
    : QWidget(parent)
{
    signalMapper = new QSignalMapper(this);

    QHBoxLayout *gridLayout = new QHBoxLayout(this);

    for(int i=0; i < paths.size(); i++){
        QPushButton *bt = new QPushButton(this);
        QImage img(paths.at(i));
        bt->setIcon( QIcon(paths.at(i)) );
        bt->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        bt->setIconSize(img.size());

        connect(bt, SIGNAL(clicked()), signalMapper, SLOT(map()) );

        signalMapper->setMapping(bt, img.text("Latex") );

        gridLayout->addWidget(bt);
    }

    gridLayout->addStretch(0);
    gridLayout->setSpacing(0);
    gridLayout->setContentsMargins(0,0,0,0);

    connect(signalMapper, SIGNAL(mapped(const QString&)), this, SIGNAL(clicked(const QString&)) );

    setLayout(gridLayout);
}
