/*
 * Copyright 2013 Mihai Niculescu <q.quark@gmail.com>
 *
 * This file is part of EqualX Project (https://launchpad.net/equalx/)
 *
 * EqualX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EqualX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is the implementation of EqualX::File when using exempi API (intended for Linux)
 */
#include <QDebug>

#include <QDateTime>
#include <QFile>
#include <QTextStream>

#include <exempi/xmp.h>
#include <exempi/xmpconsts.h>

#include "defines.h"
#include "File.h"
#include "Util.h"

namespace EqualX{

/* Theft code from GPL code pdfcropxmp.pl by Johannes Grosse - Copyright (C) 2007 */
const char* __templateLatexCropFile = "\\def\\pdffile{%1}\n"
        "\\csname pdfmapfile\\endcsname{}\n"
        "\\newread\\instream\n"
        "\\def\\includexmp#1{%\n"
        "\\openin\\instream=#1\n"
        "\\ifeof\\instream\\else \n"
        "\\closein\\instream\n"
        "\% file exists\n"
        "\\begingroup\n"
        "\\pdfcompresslevel=0\n"
        "\\immediate\\pdfobj stream attr {/Type /Metadata /Subtype /XML}\n"
        "file{#1}\n"
        "\\pdfcatalog{/Metadata \\the\\pdflastobj\\space 0 R}\n"
        "\\endgroup \%/\n"
        "\\fi\n"
        "}\n"
        "\\def\\page #1 [#2 #3 #4 #5]{\%\n"
        "\\count0=#1\\relax\n"
        "\\setbox0=\\hbox{\%\n"
        "\\pdfximage page #1{\\pdffile}\%\n"
        "\\pdfrefximage\\pdflastximage\n"
        "}\%\n"
        "\\pdfhorigin=-#2bp\\relax\n"
        "\\pdfvorigin=#3bp\\relax\n"
        "\\pdfpagewidth=#4bp\\relax\n"
        "\\advance\\pdfpagewidth by -#2bp\\relax\n"
        "\\pdfpageheight=#5bp\\relax\n"
        "\\advance\\pdfpageheight by -#3bp\\relax\n"
        "\\ht0=\\pdfpageheight\n"
        "\\shipout\\box0\\relax\n"
        "}\n"
        "\\def\\pageclip #1 [#2 #3 #4 #5][#6 #7 #8 #9]{\%\n"
        "\\count0=#1\\relax\n"
        "\\ndimen0=#4bp\\relax \\advance\\dimen0 by -#2bp\\relax\n"
        "\\edef\\imagewidth{\\the\\dimen0}\%\n"
        "\\dimen0=#5bp\\relax \\advance\\ndimen0 by -#3bp\\relax\n"
        "\\edef\\imageheight{\\the\\dimen0}\%\n"
        "\\pdfximage page #1{\\pdffile}\%\n"
        "\\setbox0=\\hbox{\%\n"
        "\\kern -#2bp\\relax\n"
        "\\lower #3bp\\hbox{\\pdfrefximage\\pdflastximage}\%\n"
        "}\%\n"
        "\\wd0=\\imagewidth\\relax\n"
        "\\ht0=\\imageheight\\relax\n"
        "\\dp0=0pt\\relax\n"
        "\\pdfhorigin=#6pt\\relax\n"
        "\\pdfvorigin=#7bp\\relax\n"
        "\\pdfpagewidth=\\imagewidth\n"
        "\\advance\\pdfpagewidth by #6bp\\relax\n"
        "\\advance\\pdfpagewidth by #8bp\\relax\n"
        "\\pdfpageheight=\\imageheight\\relax\n"
        "\\advance\\pdfpageheight by #7bp\\relax\n"
        "\\advance\\pdfpageheight by #9bp\\relax\n"
        "\\pdfxform0\\relax\n"
        "\\shipout\\hbox{\\pdfrefxform\\pdflastxform}\%\n"
        "}\%\n"
        "\\includexmp{%2}\n"
        "\\page %3 [%4 %5 %6 %7]\n"
        "\\csname @@end\\endcsname\n\\end\n";

/*
 * Identifiers for __templateLatexCropFile:
 *  - input pdf file
 *  - input xmp metadata file
 *  - page number
 *  - llx: lower left x
 *  - lly: lower left y
 *  - urx: upper right x
 *  - ury: upper right y
 * (gs has the coordinate system with origin in the lower left corner)
 */

/* End Theft */


const char* __templatePSwithXMP = "/currentdistillerparams where\n"
        "{pop currentdistillerparams /CoreDistVersion get 5000 lt} {true} ifelse\n"
        "{ userdict /pdfmark /cleartomark load put\n"
        "userdict /metafile_pdfmark {flushfile cleartomark } bind put}\n"
        "{ userdict /metafile_pdfmark {/PUT pdfmark} bind put} ifelse\n"
        "[/_objdef {%2} /type /stream /OBJ pdfmark\n"
        "[{%2}\n"
        "currentfile 0 (% &&end XMP packet marker&&)\n"
        "/SubFileDecode filter metafile_pdfmark\n"
        "<?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d'?>\n"
        "%1\n"
        "<?xpacket end='r'?>\n"
        "% &&end XMP packet marker&&\n"
        "[{%2}\n"
        "2 dict begin /Type /Metadata def /Subtype /XML currentdict end\n"
        "/PUT pdfmark\n";

/*
 * Identifiers for __templatePSwithXMP:
 *  %1 - the XMP packet
 *  %2 - equalx_metadata_stream dependant on timestamp
 */
}

typedef struct _Xmp *XmpPtr;
typedef struct _XmpFile *XmpFilePtr;

struct EqualX::File::FileImplementation{
    XmpPtr mMetadata;
    XmpFilePtr mFile;
};

EqualX::File::File() :
   mFileName(),
    mFileInfo(0)
{
    __init();
}

EqualX::File::File(const EqualX::FileInfo &fileinfo)
    : mFileName(),
      mFileInfo(fileinfo)
{
    __init();
}

EqualX::File::~File()
{
    xmp_free(mClassImpl->mMetadata);
    xmp_terminate();

    delete mClassImpl;
}


void EqualX::File::__init()
{
    xmp_init();

    mClassImpl = new FileImplementation;
    mClassImpl->mMetadata = xmp_new_empty(); // new empty XMP packet pointer
}


void EqualX::File::open(const QString &filename, EqualX::File::OpenModes mode)
{
   mFileName = filename;
    qDebug() << "[EqualX::File::open] Openning file:"<<filename<<" in mode:";

    XmpOpenFileOptions xmpOpenMode;
    if(mode.testFlag(EqualX::File::OPEN_READ)){
        xmpOpenMode = XMP_OPEN_READ;
        qDebug() << "read";
    }

    if(mode.testFlag(EqualX::File::OPEN_UPDATE)){
        xmpOpenMode = (XmpOpenFileOptions)XMP_OPEN_FORUPDATE;
        qDebug() << "update";
    }

    if(mode.testFlag(EqualX::File::OPEN_SMART)){
        xmpOpenMode = (XmpOpenFileOptions)(xmpOpenMode | XMP_OPEN_USESMARTHANDLER);
        qDebug() << "smart";
    }

    if(mode.testFlag(EqualX::File::OPEN_SCAN)){
        xmpOpenMode = (XmpOpenFileOptions)(xmpOpenMode | XMP_OPEN_USEPACKETSCANNING);
        qDebug() << "scan";

    }

    mClassImpl->mFile = xmp_files_open_new(filename.toLatin1().constData(), (XmpOpenFileOptions)(xmpOpenMode) );
}

void EqualX::File::close()
{
    xmp_files_close(mClassImpl->mFile,XMP_CLOSE_NOOPTION);
    xmp_files_free(mClassImpl->mFile);
}

bool EqualX::File::read()
{
    xmp_free(mClassImpl->mMetadata);
    mClassImpl->mMetadata = xmp_new_empty();

    if(!xmp_files_get_xmp(mClassImpl->mFile, mClassImpl->mMetadata)){
        qDebug() << "[EqualX::File::read] Can not retrieve XMP metadata from file. Aborting read...";
        return false;
    }

    if(!xmp_namespace_prefix(METADATA_NS, NULL) ){
        qDebug() << "[EqualX::File::read] Namespace ["<<METADATA_NS<<"] is not registered. Aborting read...";
        return false;
    }

    if(!xmp_prefix_namespace_uri(METADATA_PREFIX, NULL) ){
        qDebug() << "[EqualX::File::read] Prefix ["<<METADATA_PREFIX<<"] is not registered. Aborting read...";
        return false;
    }

    XmpStringPtr preamble= xmp_string_new();
    XmpStringPtr equation= xmp_string_new();
    XmpStringPtr fgColor = xmp_string_new();
    XmpStringPtr bgColor = xmp_string_new();
    int environment;
    int fontSize;

    bool readStatus = true;
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_PREAMBLE, preamble,0);
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_EQUATION, equation,0);
    readStatus = xmp_get_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_ENV, &environment, 0);
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_FG, fgColor, 0);
    readStatus = xmp_get_property(mClassImpl->mMetadata, METADATA_NS, METADATA_BG, bgColor, 0);
    readStatus = xmp_get_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_DOC_FONT_SIZE, &fontSize, 0);

    QString preambleStr = xmp_string_cstr(preamble);
    preambleStr.replace( "#perc", "%");
    preambleStr.replace( "#", "\\");

    QString equationStr = xmp_string_cstr(equation);
    equationStr.replace("#perc", "%");
    equationStr.replace( "#", "\\");

    QColor c1;
    QColor c2;

    EqualX::Util::LatexToColor(xmp_string_cstr(fgColor), c1);
    EqualX::Util::LatexToColor(xmp_string_cstr(bgColor), c2);

    mFileInfo.setPreamble(preambleStr);
    mFileInfo.setEquation(equationStr);
    mFileInfo.setForegroundColor( c1 );
    mFileInfo.setBackgroundColor( c2 );
    mFileInfo.setEnvironment(environment);
    mFileInfo.setFontSize(fontSize);

    xmp_string_free(preamble);
    xmp_string_free(equation);
    xmp_string_free(fgColor);
    xmp_string_free(bgColor);

    return readStatus;

}

bool EqualX::File::write()
{
    bool status = false;
    QString filetype = mFileName.section(".",-1);

    qDebug() << "[EqualX::File::write] Trying to insert metadata into "<<mFileName<<" ext:"<<filetype;

    if(filetype.contains("svg") ){
        qDebug() << "[EqualX::File::write] \tSeriliazing metadata";

        XmpStringPtr buf = xmp_string_new();
        xmp_serialize(mClassImpl->mMetadata, buf, XMP_SERIAL_OMITPACKETWRAPPER, 0);
        QString metadataStr(xmp_string_cstr(buf));

        qDebug() << "[EqualX::File::write] \tOpenning file";
        QFile file( mFileName );
        file.open(QIODevice::ReadWrite | QIODevice::Text);
        QTextStream stream(&file);

         qDebug() << "[EqualX::File::write] \tSearching for EOF";
        QString line;
        qint64 pos;
        do{
            pos = stream.pos();
            line = stream.readLine();
        } while (!line.contains("</svg>"));

         qDebug() << "[EqualX::File::write] \tFound EOF. Flushing metadata...";
        stream.seek(pos);
        stream << "<![CDATA[\n" << "<?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d'?>\n"<<metadataStr <<"<?xpacket end='r'?>"<< "]]>\n";
        stream << "</svg>";
        stream.flush();
        file.close();
        xmp_string_free(buf);
         qDebug() << "[EqualX::File::write] \tFlushing Done.";
        status = true;

    }
    else if(filetype.contains("ps")){ // for ps and eps documents, level 2
        XmpStringPtr buf = xmp_string_new();
        xmp_serialize(mClassImpl->mMetadata, buf, XMP_SERIAL_OMITPACKETWRAPPER, 0);
        QString metadataStr(xmp_string_cstr(buf));

        QFile in(mFileName); QFile out(mFileName+"~");
        in.open(QIODevice::ReadOnly); out.open(QIODevice::WriteOnly);
        QTextStream stream(&out);

        QString line;
        QString curTimeStamp = QString::number(QDateTime::currentDateTime().toTime_t());
        while(!in.atEnd()){
            line = in.readLine();
            stream << line;
            if(line.contains("%%BeginPageSetup")){
                stream << QString(__templatePSwithXMP).arg(metadataStr, curTimeStamp);
            }
            stream.flush();
        }

        in.close();
        out.close();

        QFile::remove(mFileName);
        QFile::rename(mFileName+"~", mFileName);
        xmp_string_free(buf);

        status = true;
    }
    else if(xmp_files_can_put_xmp(mClassImpl->mFile, mClassImpl->mMetadata)){
        status=xmp_files_put_xmp(mClassImpl->mFile, mClassImpl->mMetadata);
    }
    else{
        status = false;
    }

    if(status) qDebug() << "[EqualX::File::write] inserted metadata to file:" << mFileName;

    return status;
}

void EqualX::File::writeLatexFile(bool withBackgroundColor)
{
    QString latexColors; // sets font color
    QString latexHLColor; // sets HighLight color

    QString latexFGColor, latexBGColor;

    EqualX::Util::colorToLatex(mFileInfo.fgColor(), latexFGColor);
    EqualX::Util::colorToLatex(mFileInfo.bgColor(), latexBGColor);

    latexColors = QString("\\definecolor{fgC}{rgb}{%1}\\color{fgC}").arg(latexFGColor);
    if(withBackgroundColor){
        QString bgColor = QString("\\definecolor{bgC}{rgb}{%1}\\pagecolor{bgC} ").arg(latexBGColor);
        latexColors.append(bgColor);
    }

    QString equation = mFileInfo.equation(); // we modify it if we have selections

    /* Highlight selections */
    if(!mFileInfo.selections().isEmpty()){
        if(withBackgroundColor){
            latexHLColor = QString("\\definecolor{selC}{rgb}{%1}").arg(latexBGColor);
        }
        else {
            latexHLColor = QString("\\definecolor{selC}{rgb}{1,1,1}");
        }

        QString insert1 = "\\colorbox{fgC}{\\color{selC} $";
        QString insert2 = "$}";

        int insertStr1Len = insert1.length();
        int insertStr2Len = insert2.length();
        int insertStrLen = insertStr1Len + insertStr2Len;
        int newInsert1Pos, newInsert2Pos;

        for(int i=0; i < mFileInfo.selections().size(); i++){
            SelectionIndex *selectionItem = mFileInfo.selections().at(i);

            int ss = selectionItem->start; // start selection
            int se = selectionItem->end; // end selection

            newInsert1Pos = ss + i*insertStrLen;
            newInsert2Pos = se + i*insertStrLen + insertStr1Len;

            equation.insert(newInsert1Pos, insert1);
            equation.insert(newInsert2Pos, insert2);
        }

    }

    /* Set Font Size */
    QString latexFontSize;
    switch(mFileInfo.fontSize()){
    case LATEX_FONT_TINY:
        latexFontSize="\\tiny ";
        break;
    case LATEX_FONT_SCRIPT:
        latexFontSize="\\scriptsize ";
        break;
    case LATEX_FONT_FOOTNOTE:
        latexFontSize="\\footnotesize ";
        break;
    case LATEX_FONT_SMALL:
        latexFontSize="\\small ";
        break;
    default:
    case LATEX_FONT_NORMAL:
        latexFontSize="";
        break;
    case LATEX_FONT_LARGE:
        latexFontSize="\\large ";
        break;
    case LATEX_FONT_VERY_LARGE:
        latexFontSize="\\LARGE ";
        break;
    case LATEX_FONT_HUGE:
        latexFontSize="\\huge ";
        break;
    case LATEX_FONT_VERY_HUGE:
        latexFontSize="\\Huge ";
        break;

    }

    /* Set Environment */
    QString envBegin, envEnd;
    switch(mFileInfo.environment()){
    default:
    case LATEX_ENV_MODE_DISPLAY:
        envBegin = "\\[";
        envEnd = "\\]";
        break;
    case LATEX_ENV_MODE_INLINE:
        envBegin = "$";
        envEnd = "$";
        break;
    case LATEX_ENV_MODE_ALIGN:
        envBegin = "\\begin{align*}";
        envEnd = "\\end{align*}";
        break;
    case LATEX_ENV_MODE_TEXT:
        envBegin = "";
        envEnd = "";
        break;
    }

    // compose whole content of latex file
    QString latexFileContent = mFileInfo.preamble()+"\n\\begin{document}"+latexFontSize+latexColors+latexHLColor+envBegin+equation+envEnd+"\\end{document}";

    // write the content to file
    QFile texFile( TEMP_LATEX_FILE );
    texFile.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&texFile);
    out << latexFileContent;
    out.flush();
    texFile.close();
}

void EqualX::File::writeLatexFileCropped(float llx, float lly, float urx, float ury)
{
    qDebug() << "[File::writeLatexFileCropped] Boundingbox: ("<<llx<<","<<lly<<","<<urx<<","<<ury<<")";

    QString texFileContents = QString(EqualX::__templateLatexCropFile).arg(TEMP_PDF_FILE,TEMP_METADATA_FILE,QString::number(1),
                                                                           QString::number(llx), QString::number(lly),
                                                                           QString::number(urx), QString::number(ury));

    // write generated LaTeX to a temp file
    QFile tmpTexFile( TEMP_LATEX_CROP_FILE );
    tmpTexFile.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&tmpTexFile);
    out << texFileContents;
    out.flush();
    tmpTexFile.close();
}

void EqualX::File::writeMetadataFile()
{
    XmpStringPtr buf = xmp_string_new();

    xmp_serialize(mClassImpl->mMetadata, buf, XMP_SERIAL_OMITPACKETWRAPPER, 0);

    // write generated metadata to file
    QFile metadatafile( TEMP_METADATA_FILE );
    metadatafile.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&metadatafile);
    out << "<?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d'?>\n";
    out << xmp_string_cstr(buf);
    out << "<?xpacket end='r'?>";
    out.flush();
    metadatafile.close();

    xmp_string_free(buf);

}

bool EqualX::File::fetchInfo(const QString &filename, EqualX::FileInfo *info)
{
    if(!info) info = new EqualX::FileInfo;

    QString fileExt = filename.section(".", -1);

    EqualX::File::OpenModes mode = EqualX::File::OPEN_READ;

    if(fileExt.contains("svg") || fileExt.contains("ps")){
        mode = EqualX::File::OPEN_READ | EqualX::File::OPEN_SCAN;
    }

    EqualX::File f;
    f.open(filename, mode);
    bool readStatus = f.read();
    if(readStatus)
    {
        *info = f.info();
    }

    f.close();


    return readStatus;
}

bool EqualX::File::fetchInfo(const char *buffer, size_t len, EqualX::FileInfo *info)
{
    xmp_init();
    XmpPtr metadata = xmp_new(buffer,len);

    if(!xmp_namespace_prefix(METADATA_NS, NULL) ){
        qDebug() << "[EqualX::File::read] Namespace ["<<METADATA_NS<<"] is not registered. Aborting read...";
        return false;
    }

    if(!xmp_prefix_namespace_uri(METADATA_PREFIX, NULL) ){
        qDebug() << "[EqualX::File::read] Prefix ["<<METADATA_PREFIX<<"] is not registered. Aborting read...";
        return false;
    }

    XmpStringPtr preamble= xmp_string_new();
    XmpStringPtr equation= xmp_string_new();
    XmpStringPtr fgColor = xmp_string_new();
    XmpStringPtr bgColor = xmp_string_new();
    int environment;
    double fontSize;

    bool readStatus = true;
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_PREAMBLE, preamble,0);
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_EQUATION, equation,0);
    readStatus = xmp_get_property_int32(metadata, METADATA_NS, METADATA_ENV, &environment, 0);
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_FG, fgColor, 0);
    readStatus = xmp_get_property(metadata, METADATA_NS, METADATA_BG, bgColor, 0);
    readStatus = xmp_get_property_float(metadata, METADATA_NS, METADATA_DOC_FONT_SIZE, &fontSize, 0);

    QString preambleStr = xmp_string_cstr(preamble);
    preambleStr.replace( "#perc", "%");
    preambleStr.replace( "#", "\\");

    QString equationStr = xmp_string_cstr(equation);
    equationStr.replace("#perc", "%");
    equationStr.replace( "#", "\\");

    QColor c1;
    QColor c2;

    EqualX::Util::LatexToColor(xmp_string_cstr(fgColor), c1);
    EqualX::Util::LatexToColor(xmp_string_cstr(bgColor), c2);

    info->setPreamble(preambleStr);
    info->setEquation(equationStr);
    info->setForegroundColor( c1 );
    info->setBackgroundColor( c2 );
    info->setEnvironment(environment);
    info->setFontSize((int)fontSize);

    xmp_string_free(preamble);
    xmp_string_free(equation);
    xmp_string_free(fgColor);
    xmp_string_free(bgColor);

    xmp_free(metadata);
    xmp_terminate();

    return readStatus;
}

EqualX::FileInfo EqualX::File::info() const
{
    return mFileInfo;
}

void EqualX::File::setInfo(const EqualX::FileInfo &fileinfo)
{
    mFileInfo = fileinfo;

    xmp_free(mClassImpl->mMetadata);

    mClassImpl->mMetadata = xmp_new_empty(); // new packet pointer

    XmpStringPtr prefix = xmp_string_new(); // actual prefix

    xmp_register_namespace(METADATA_NS, METADATA_PREFIX, prefix);
    xmp_string_free(prefix);

    // escape chars
    QString preamble = mFileInfo.preamble();
    preamble.replace("%", "#perc");
    preamble.replace("\\", "#");

    QString eq = mFileInfo.equation();
    eq.replace("%", "#perc");
    eq.replace("\\", "#");

    QString fgCol, bgCol;

    EqualX::Util::colorToLatex(mFileInfo.fgColor(), fgCol);
    EqualX::Util::colorToLatex(mFileInfo.bgColor(), bgCol);

    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_PREAMBLE, preamble.toStdString().c_str(), 0);
    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_EQUATION, eq.toStdString().c_str(), 0);
    xmp_set_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_ENV, mFileInfo.environment(), 0);
    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_FG, fgCol.toLatin1().constData(), 0);
    xmp_set_property(mClassImpl->mMetadata, METADATA_NS, METADATA_BG, bgCol.toLatin1().constData(), 0);
    xmp_set_property_int32(mClassImpl->mMetadata, METADATA_NS, METADATA_DOC_FONT_SIZE, mFileInfo.fontSize(), 0);
}
